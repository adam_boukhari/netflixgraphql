package ca.csfoy.servicesweb.netflix.domaine.actor;

import java.time.LocalDate;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

public class Actor {
	
	private String id;
	private String firstname;
	private String lastname;
	private LocalDate birthdate;
	private String bio;
	
	public Actor(String id, String firstname, String lastname, LocalDate birthdate, String bio) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.birthdate = birthdate;
		this.bio = bio;
	}

	public String getId() {
		return id;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public String getBio() {
		return bio;
	}

	public boolean isMatching(String actor) {
		if (!StringUtils.isBlank(actor)) {
			return (firstname.contains(actor) || lastname.contains(actor));
		}
		return true;
	}

	public boolean isMatchingExactly(String firstname, String lastname) {
		if (!StringUtils.isBlank(firstname)) {
			if (!StringUtils.isBlank(lastname)) {
				return (this.firstname.equalsIgnoreCase(firstname) && this.lastname.equalsIgnoreCase(lastname));
			} else {
				return this.firstname.equalsIgnoreCase(firstname);
			}
		} else {
			if (!StringUtils.isBlank(lastname)) {
				return this.lastname.equalsIgnoreCase(lastname);
			}
		}
		return true;
	}

	public boolean isDuplicate(Actor actor) {
		return Objects.equals(actor.getFirstname().toLowerCase(), this.firstname.toLowerCase())
				&& Objects.equals(actor.getLastname().toLowerCase(), this.lastname.toLowerCase()) 
				&& Objects.equals(actor.getBirthdate(), this.birthdate);
	}
}

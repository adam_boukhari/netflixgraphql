package ca.csfoy.servicesweb.netflix.infra.actor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ActorDao extends JpaRepository<ActorEntity, String> {
	
	@Query("SELECT a FROM ActorEntity a WHERE a.firstname LIKE %?1% AND a.lastname LIKE %?2%")
	ActorEntity findByFirstnameAndLastname(String firstname, String lastname);
	
}

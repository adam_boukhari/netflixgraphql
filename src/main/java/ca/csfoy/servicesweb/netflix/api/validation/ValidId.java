package ca.csfoy.servicesweb.netflix.api.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;


@Target({ElementType.FIELD, ElementType.PARAMETER})
@Constraint(validatedBy={})
@Retention(RetentionPolicy.RUNTIME)
@Pattern(message="L'identifiant doit correspondre à un UUID.", regexp="^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$")
@NotBlank(message = "L'identifiant est obligatoire et doit correspondre à un UUID.")//Ajout du message ici car le message original de NotBlank prenait le dessus.
public @interface ValidId {
	
    String message() default "L'identifiant est obligatoire et doit correspondre à un UUID.";
    
    Class<?>[] groups() default {};
    
    Class<? extends Payload>[] payload() default {};
}

package ca.csfoy.servicesweb.netflix.api.actor;

import java.time.LocalDate;
import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import ca.csfoy.servicesweb.netflix.api.ActorDto;
import ca.csfoy.servicesweb.netflix.controller.ActorConverter;
import ca.csfoy.servicesweb.netflix.domaine.actor.Actor;
import ca.csfoy.servicesweb.netflix.domaine.actor.ActorRepository;


@Tag("Api")
//Bah ça fonctionnais pas, Il y avais rien dans vos démo qui parlaient de ça donc je suis allé voir sur internet :shrug:
// https://github.com/graphql-java-kickstart/graphql-spring-boot/issues/146
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT) 
@AutoConfigureMockMvc
public class ActorResourceTest {
	
	
	private static final String PATH_TO_TEST = "/graphql";
	private static final String ANY_ID = UUID.randomUUID().toString();
	private static final String ANY_FIRSTNAME = "Scarlett";
	private static final String ANY_LASTNAME = "Johansson";
	private static final String ANY_BIO = "Bio drando";
	private static final String ANY_BIRTHDATE = LocalDate.now().minusYears(28).toString();
	private static final LocalDate ANY_BIRTH_LOCAL_DATE = LocalDate.now().minusYears(28);
	private static final String GET_QUERY_TEMPLATE = "query { getActor(id: \"%s\") { id firstname lastname birthdate bio }}";
	
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@MockBean
	private ActorRepository repo;
	
	@MockBean 
	private ActorConverter converter;
	
	@Test
	public void get_ifGetSuccessful_thenReturn200Ok() throws Exception {

	}
	
	@Test
	public void get_ifActorDoesNotExist_thenReturn404Ok() throws Exception {
	}
}

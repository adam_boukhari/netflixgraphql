package ca.csfoy.servicesweb.netflix.controller;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import ca.csfoy.servicesweb.netflix.api.ActorDto;
import ca.csfoy.servicesweb.netflix.domaine.actor.Actor;
import ca.csfoy.servicesweb.netflix.domaine.actor.ActorRepository;

@Tag("Unitaire")
@ExtendWith(MockitoExtension.class)
public class ActorControllerTest {
	
	private static final String ANY_ID = "1";
	private static final String ANY_OTHER_ID = "2";
	
	@Mock
	private ActorConverter converter;
	
	@Mock
	private ActorRepository repository;
	
	@InjectMocks
	private ActorController controller;
	
	@Test
	public void getAll_thenAllActorsReturned() {
		ActorDto dto1 = new ActorDto(ANY_ID, "Scarlett", "Johansson", LocalDate.now().minusYears(28).toString(), "");
		ActorDto dto2 = new ActorDto(ANY_OTHER_ID, "Nathalie", "Portman", LocalDate.now().minusYears(29).toString(), "");
		List<ActorDto> dtos = Arrays.asList(dto1, dto2);
		Actor actor1 = new Actor(ANY_ID, "Scarlett", "Johansson", LocalDate.now().minusYears(28), "");
		Actor actor2 = new Actor(ANY_OTHER_ID, "Nathalie", "Portman", LocalDate.now().minusYears(29), "");
		List<Actor> actors = Arrays.asList(actor1, actor2);
		Mockito.when(repository.getAll()).thenReturn(actors);
		Mockito.when(converter.fromActorList(actors)).thenReturn(dtos);
		
		List<ActorDto> actorsReturned = controller.getActors();
		
		Mockito.verify(repository).getAll();
		Mockito.verify(converter).fromActorList(actors);
		Assertions.assertEquals(dtos, actorsReturned);		
	}
	
	@Test
	public void getBy_ifIdValid_thenActorReturned() {
		ActorDto dto1 = new ActorDto(ANY_ID, "Scarlett", "Johansson", LocalDate.now().minusYears(28).toString(), "");
		Actor actor1 = new Actor(ANY_ID, "Scarlett", "Johansson", LocalDate.now().minusYears(28), "");
		Mockito.when(repository.getBy(ANY_ID)).thenReturn(actor1);
		Mockito.when(converter.fromActor(actor1)).thenReturn(dto1);
		
		ActorDto actorReturned = controller.getActor(ANY_ID);
		
		Mockito.verify(repository).getBy(ANY_ID);
		Mockito.verify(converter).fromActor(actor1);
		Assertions.assertEquals(dto1, actorReturned);		
	}
	
	@Test
	public void create_ifInputValid_thenActorCreated() {
		ActorDto dto = new ActorDto(ANY_ID, "Scarlett", "Johansson", LocalDate.now().minusYears(28).toString(), "");
		Actor actor = new Actor(ANY_ID, "Scarlett", "Johansson", LocalDate.now().minusYears(28), "");
		Mockito.when(converter.toActorCreation(dto)).thenReturn(actor);
		
		controller.createActor(dto);
		
		Mockito.verify(converter).toActorCreation(dto);
		Mockito.verify(repository).create(actor);
	}
	
	@Test
	public void update_ifInputValid_thenActorUpdated() {
		ActorDto dto = new ActorDto(ANY_ID, "Scarlett", "Johansson", LocalDate.now().minusYears(28).toString(), "");
		Actor actor = new Actor(ANY_ID, "Scarlett", "Johansson", LocalDate.now().minusYears(28), "");
		Mockito.when(converter.toActor(dto)).thenReturn(actor);
		
		controller.updateActor(dto);
		
		Mockito.verify(converter).toActor(dto);
		Mockito.verify(repository).save(actor);
	}
	
	@Test
	public void delete_ifInputValid_thenActorDeleted() {		
		controller.deleteActor(ANY_ID);
		
		Mockito.verify(repository).delete(ANY_ID);
	}
}

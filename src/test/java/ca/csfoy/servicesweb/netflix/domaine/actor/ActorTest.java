package ca.csfoy.servicesweb.netflix.domaine.actor;

import java.time.LocalDate;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import ca.csfoy.servicesweb.netflix.domaine.ActorMother;

import ca.csfoy.servicesweb.netflix.domaine.actor.Actor;

@Tag("Unitaire")
public class ActorTest {

	private static Stream<Arguments> isMatchingActorNameArguments() {
		return Stream.of(
				Arguments.of(null, true),
				Arguments.of("", true),
				Arguments.of("  ", true),
				Arguments.of(ActorMother.ANY_FIRSTNAME, true), 
				Arguments.of(ActorMother.ANY_LASTNAME, true),
				Arguments.of("Scar", true), 
				Arguments.of("hansson", true),
				Arguments.of("Widow", false), 
				Arguments.of("Scarlett Johansson", false));
	}
	
	@ParameterizedTest
	@MethodSource("isMatchingActorNameArguments")
	public void isMatching_withInputActor_thenResultIsAsExpected(String actorName, boolean expectedResult) {
		Actor actor = ActorMother.getAnyActor();
		
		boolean result = actor.isMatching(actorName);
		
		Assertions.assertEquals(expectedResult, result);
	}
	
	private static Stream<Arguments> isMatchingActorNameExactlyArguments() {
		return Stream.of(
				Arguments.of(null, null, true),
				Arguments.of("", "", true),
				Arguments.of("  ", "  ", true),
				Arguments.of(ActorMother.ANY_FIRSTNAME, "  ", true),
				Arguments.of(ActorMother.ANY_FIRSTNAME, ActorMother.ANY_LASTNAME, true), 
				Arguments.of(ActorMother.ANY_FIRSTNAME.toLowerCase(), ActorMother.ANY_LASTNAME.toUpperCase(), true),
				Arguments.of("Other Name", "  ", false),
				Arguments.of("  ", "Other Name", false),
				Arguments.of("Black","Widow", false));
	}
	
	@ParameterizedTest
	@MethodSource("isMatchingActorNameExactlyArguments")
	public void isMatchingExactly_withInputActor_thenResultIsAsExpected(String firstname, String lastname, boolean expectedResult) {
		Actor actor = ActorMother.getAnyActor();
		
		boolean result = actor.isMatchingExactly(firstname, lastname);
		
		Assertions.assertEquals(expectedResult, result);
	}
	
	private static Stream<Arguments> isDuplicateActorArguments() {
		return Stream.of(
				Arguments.of(ActorMother.getAnyActor(), true),
				Arguments.of(ActorMother.getAnyOtherActor(), false),
				Arguments.of(ActorMother.getAnyActorWithFirstname(ActorMother.ANY_FIRSTNAME.toUpperCase()), true),
				Arguments.of(ActorMother.getAnyActorWithLastname(ActorMother.ANY_LASTNAME.toUpperCase()), true),
				Arguments.of(ActorMother.getAnyActorWithFirstname("Black"), false),
				Arguments.of(ActorMother.getAnyActorWithLastname("Black"), false), 
				Arguments.of(ActorMother.getAnyActorWithDate(LocalDate.of(2020, 1, 1)), false));
	}
	
	@ParameterizedTest
	@MethodSource("isDuplicateActorArguments")
	public void isDuplicate_withInputActor_thenResultIsAsExpected(Actor expectedActor, boolean expectedResult) {
		Actor actor = ActorMother.getAnyActor();
		
		boolean result = actor.isDuplicate(expectedActor);
		
		Assertions.assertEquals(expectedResult, result);
	}
}

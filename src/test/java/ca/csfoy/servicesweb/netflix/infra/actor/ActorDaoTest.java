package ca.csfoy.servicesweb.netflix.infra.actor;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@Tag("Data")
@DataJpaTest
@ExtendWith(SpringExtension.class)
public class ActorDaoTest {

	@Autowired
	private ActorDao dao;
	
	@Test
	public void getByFirstnameAndLastnanme_whenIdExists_thenActorIsReturned() {
		ActorEntity actorReturned = dao.findByFirstnameAndLastname("Scarlett", "Johansson");
		
		Assertions.assertEquals("da1dd318-db0a-4393-b5d9-b55d143f4fea", actorReturned.id);
	}
	
	@Test
	public void getByFirstnameAndLastnanme_ifOnlyFirstname_thenActorIsReturned() {
		ActorEntity actorReturned = dao.findByFirstnameAndLastname("Scarlett", "");
		
		Assertions.assertEquals("da1dd318-db0a-4393-b5d9-b55d143f4fea", actorReturned.id);
	}
	
	@Test
	public void getByFirstnameAndLastnanme_ifParOfFirstname_thenActorIsReturned() {
		ActorEntity actorReturned = dao.findByFirstnameAndLastname("carle", "");
		
		Assertions.assertEquals("da1dd318-db0a-4393-b5d9-b55d143f4fea", actorReturned.id);
	}
	
	@Test
	public void getByFirstnameAndLastnanme_ifOnlyLastname_thenActorIsReturned() {
		ActorEntity actorReturned = dao.findByFirstnameAndLastname("", "Johansson");
		
		Assertions.assertEquals("da1dd318-db0a-4393-b5d9-b55d143f4fea", actorReturned.id);
	}
	
	@Test
	public void getByFirstnameAndLastnanme_ifPartOfLastname_thenActorIsReturned() {
		ActorEntity actorReturned = dao.findByFirstnameAndLastname("", "hans");
		
		Assertions.assertEquals("da1dd318-db0a-4393-b5d9-b55d143f4fea", actorReturned.id);
	}

}
